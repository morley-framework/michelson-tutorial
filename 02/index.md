# Part II: Hello Tezos

Now that we know a little Michelson, let's take a look at the Tezos blockchain,
which is the "environment" in which our Michelson contracts are actually going
to run.

This is not going to be an exhaustive overview of Tezos; we're going to focus
on those aspects that are most relevant from the perspective of a Michelson
programmer. We're going to look at a few useful tools, such as the
`tezos-client` program that we installed in last chapter and an online
blockchain explorer.

These tools, like Tezos itself, are still immature and undergoing rapid
development. For the most part, they employ the "info-dump" style of user
interface, which works fine for e.g. a Tezos core developer, but can be very
overwhelming to newcomers. For the purposes of this tutorial, we're
specifically interested in how to program in Michelson, so we can mostly
abstract over a lot the detail exposed by the tooling that would be more
relevant in other circumstances, such as if we wanted to run a baker. Some of these
details will be covered again in greater depth in future chapters.

For now, our goal is to gain a basic understanding of how to use the current
Tezos tooling, with the goal, by the end of the next chapter, of running and
interacting with a simple Michelson contract on the Tezos test net.

## The Tezos Blockchain

Open an [JakartaNet explorer][jakartanet-expl] in your browser, go to "Blocks"
page in "BLOCKCHAIN" section and click on a hash of the first block on that
page.

You should see something like this:

![](block-overview.png)

This is an overview of the latest block in the network.

## Blockchains: A brief review

Now, click on the highlighted button to view block information.

![](block-info-highlight.png)

Look at the entry that says

```
"predecessor": "BLyYjm28TFzFyRmNJrtK9qX7h1zQ7Zm4ua1TJZRHFwrLzJ1ND4i"
```

To briefly review, a blockchain is, as the name suggests, a chain of blocks.
Each block includes a hash of its predecessor block, which in turn includes a
hash of its predecessor, and so on and so forth all the way back to the first
(or "genesis") block in the chain that has no predecessor.

![a chain of blocks](https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Blockchain.svg/150px-Blockchain.svg.png)

You can take a look at the test net Genesis block here:
https://jakartanet.tzkt.io/0

The reason each block includes the hash of its predecessor is to create a
data structure that is prohibitively difficult to surreptitiously modify.

For example, suppose I wanted to edit my copy of block `BMRiaCG...` and add a
transaction that credited one of my accounts with 1 billion tez, I could do
that.  But it would be obvious to everyone that I had tampered with the block,
because the hash of the block would no longer match.

All the information in the block (when serialized) is just a number. The hash
of the block is just the output of a specific hash function on that number:

```
Hash(serializedBlock) = BMRiaCG...
```

So if I add a transaction and change the block information, I change the
serialized number represents that block, which will change the hash of the
block. Since hash functions are designed to take negligible time and cost to
run, if I try to send my tampered copy of the block to anyone else, they can
easily recompute the hash function and discover that the hashes don't match.

Furthermore, because each block includes the hash of its predecessor, changing
any block changes all the other blocks "downstream" of it. This means that as
long as you and I agree on what the latest block in the chain (the head) is, we
can agree on the whole history of the chain. (Agreeing on the head is problem
solved by the consensus algorithm, which in Tezos is a variant of delegated
proof-of-stake).

This type of data structure, in general, is called a [hash tree or a Merkle
Tree](https://en.wikipedia.org/wiki/Merkle_tree). It's important to emphasize,
given the various abuses of the term, that all "blockchain" refers to is a hash
tree whose nodes are blocks of transactions. It is a term in the same category
as "key-value map", or "linked-list".

## Tezos Mainnet, Jakartanet, Ithacanet

Returning to our block explorer:

![](block-overview.png)

We can see that block `BMRiaCG...` only has 1 transaction in it, your specific
block may have more or less, but often they have none.  This is because the
Tezos test net is designed for testing, so transactions are fairly rare and
blocks are frequent (one every 30 seconds).  Test net tez have negligible value
and are in fact given away for free to anyone who wants them by a limitless
faucet (which we will use later on).  The test net is not decentralized and is
periodically reset from genesis every few weeks.

By now, Tezos has one main and three testing parallel blockchains.

Mainnet is where tez have real dollar value and where the network is
decentralized.

Apart from Mainnet, Tezos runs test nets, which are sometimes
superseded by the next test nets with the new protocols. Current (as of time of
writing) test nets are Jakartanet, Kathmandunet and Ghostnet.

* *Jakartanet* is running the protocol of currenly active mainnet
* *Kathmandunet* is running the (proposed) upcoming protocol
* *Ghostnet* is a long-running testnet that is currently running the `jakarta`
   protocol but will switch to different ones like Mainnet

More Information about the test networks can be found [here][docs-testnetworks].

If when you are reading this Jakartanet is already shut down, just change all
the links to whatever is active now.


Block explorers for Mainnet and Jakartanet can be found here:
  * [Mainnet block explorer][mainnet-expl] (here you can find blocks with actual transactions)
  * [JakartaNet block explorer][jakartanet-expl]

For this tutorial we'll do most of our work on the test net, although in a
later chapter we'll go through bootstrapping our own test network with a new
genesis block.

If you want you can find more information on Tezos documentation's about
[Old Test Networks](http://tezos.gitlab.io/introduction/test_networks.html#old-networks).

## Activate an account with $$ Free Money $$

The Tezos test net faucet is a website run by the Tezos developers that lets us
download a wallet file. Essentially, whenever the devs reset the test net, they
include a bunch of funded accounts in the genesis block and then give the keys
to those accounts away for free to anyone who wants to do some test net
testing.

Navigate to https://teztnets.xyz/ and download the file from the faucet for the required testnet
(note that faucets for different testnets may not overlap, i.e. you will not be able to activate
an account from a single file on multiple testnets).

Now cat the contents of that file:

```
$ cat tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ.json
{
	"pkh": "tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ",
	"mnemonic": [
      "xxxx",
      "xxxx",
      "xxxx",
      "xxxx",
      "xxxx",
      "xxxx",
      "xxxx",
      "xxxx",
      "xxxx",
      "xxxx",
      "xxxx",
      "xxxx",
      "xxxx",
      "xxxx",
      "xxxx",
	],
	"email": "iceaggtx.rjudbokx@teztnets.xyz",
	"password": "xxxx",
	"amount": "109145077629",
	"activation_code": "ad962375f53d987be64b25633d674e074e0f8092"
}
```

The two lines of relevance for us are:

```
  "amount": "109145077629",
  "pkh": "tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ",
```

This wallet file has a cryptographic secret key.
After activating the account listed in this file, the way you're going to tell
the network that you want to make a transfer is by broadcasting a
transaction message (signed by this secret key) over the peer-to-peer network.
Other peers can verify the correctness of transactions (whether we have enough
tez to make a transfer) by validating our signature with our account's
public-key, which is the "address" of the account, as well as by looking up our
balance in the blockchain.

Now let's activate the account, giving it the local nickname `alice` (don't forget to connect to corresponding node):

```
$ tezos-client activate account alice with tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ.json
```

This should output:

```
Node is bootstrapped.
Operation successfully injected in the node.
Operation hash is 'ooMJM6N3PzdJGAk8nxrTWWSfCWWoNpdouWcWgTPdADF5TnH64GA'
Waiting for the operation to be included...
Operation found in block: BLD3MKdNvsL4p8fty8WLYBXG7EUR4c8jjMiZhupzVXs7cKBfRVE (pass: 2, offset: 0)
This sequence of operations was run:
  Genesis account activation:
    Account: tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ
    Balance updates:
      tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ ... +ꜩ14255.664844

The operation has only been included 0 blocks ago.
We recommend to wait more.
Use command
  tezos-client wait for ooMJM6N3PzdJGAk8nxrTWWSfCWWoNpdouWcWgTPdADF5TnH64GA to be included --confirmations 5 --branch BM3efe5g6RUyjq5mVDdSnPKeacuZvRSKR9VffjY8A1Cw9jk6tsJ
and/or an external block explorer.
Account alice (tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ) activated with ꜩ14255.664844.
```

Let's open up the block explorer and take a look at block's
`BLD3MKdNvsL4p8fty8WLYBXG7EUR4c8jjMiZhupzVXs7cKBfRVE` ANONYMOUS tab:

![](alice-activation.png)

And now if we click on `tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ`, we should see the
account:

![](alice-account.png)

with the balance!

(It can take up to a few minutes for transactions to appear in the block
explorer, so if you don't see your transaction just sit tight for a bit while
more blocks get confirmed and try again.)

## Generating a new account

Now lets generate a brand new account called "bob" with no balance of tez:

```
$ tezos-client gen keys bob
```

This should just output a warning, but we can see the accounts we've
generated or activated in this file:

```
$ cat ~/.tezos-client/public_key_hashs
[ { "name": "bob", "value": "tz1V2PiBobwPxfZmY3Gbja5zX8qdKjn3T2Lk" },
  { "name": "alice", "value": "tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ" } ]
```

The client provides a better interface to the internal stored data, however:

```
$ tezos-client list known addresses

bob: tz1V2PiBobwPxfZmY3Gbja5zX8qdKjn3T2Lk (unencrypted sk known)
alice: tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ (unencrypted sk known)
```

## Transferring tez from alice to bob

Lets check the balances for `alice` and `bob`:

```
$ tezos-client get balance for alice

14255.664844 ꜩ
$ tezos-client get balance for bob

0 ꜩ
```

Let's transfer 120 tokens to bob from `alice`:

```
$ tezos-client transfer 120.5 from alice to bob --fee 0.5
Fatal error:
  Requested operation requires to perform a public key revelation beforehand.
  This cannot be done automatically when a custom fee or storage limit is given.
  If you wish to use a custom fee or storage limit, please first perform the
  reveal operation separately using the dedicated command.
  Otherwise, please do not specify custom fee or storage parameters.
```

Before our first operation on the blockchain, we need to perform a public key
revelation. This commits the public key for the account into the
blockchain so that other nodes can verify your signatures.
As specified in the error message, the reveal would've been done automatically
by tezos-client if we didn't specify a `fee`.

To reveal the key for alice, we run:

```
$ tezos-client reveal key for alice
Node is bootstrapped.
Estimated gas: 1000 units (will add 100 for safety)
Operation successfully injected in the node.
Operation hash is 'ooz7uNtrf9nZ72mwNe1Hq5CzJ3tUqWFNjhAFKaadwoCLVc6fzky'
Waiting for the operation to be included...
Operation found in block: BL18D6W2nAF4cmePdczFLevYoh6TaYewKxT7E8UypXuMLNtejwv (pass: 3, offset: 0)
This sequence of operations was run:
  Manager signed operations:
    From: tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ
    Fee to the baker: ꜩ0.00037
    Expected counter: 3721193
    Gas limit: 1100
    Storage limit: 0 bytes
    Balance updates:
      tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ ............. -ꜩ0.00037
      fees(tz1MeT8NACB8Q4uV9dPQ3YxXBmYgapbxQxQ5,167) ... +ꜩ0.00037
    Revelation of manager public key:
      Contract: tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ
      Key: edpkun3kPbajni7qMmL8aJ8i9CxLHm9D4RRQne5peah7BaC7ozJk3h
      This revelation was successfully applied
      Consumed gas: 1000

The operation has only been included 0 blocks ago.
We recommend to wait more.
Use command
  tezos-client wait for ooz7uNtrf9nZ72mwNe1Hq5CzJ3tUqWFNjhAFKaadwoCLVc6fzky to be included --confirmations 5 --branch BMJQnADL8dhvtxFH7q2JieYUbQUdHMjQHvyPAwTUokcMdpkthK9
and/or an external block explorer.
```

Note the `Balance updates` -- in this example we spent ꜩ0.00037 on the key
revelation (the exact fee may vary). We could also specify the fee explicitly
here via `--fee`.

Now let's try the transfer again:

```
$ tezos-client transfer 120.5 from alice to bob --fee 0.5
Fatal error:
  The operation will burn ꜩ0.06425 which is higher than the configured burn cap (ꜩ0).
   Use `--burn-cap 0.06425` to emit this operation.
```

There's `burn-cap` setting in the client to prevent us from accidentally sending
a transaction that would burn more tez than want:

```
$ tezos-client transfer 120.5 from alice to bob --fee 0.5 --burn-cap 0.06425
Node is bootstrapped.
Estimated gas: 1420 units (will add 100 for safety)
Estimated storage: 257 bytes added (will add 20 for safety)
Operation successfully injected in the node.
Operation hash is 'ooXoUpV6mF6yqeUbwX46BM8RSiLV2iAkmodhtjuaUSLcekX5W5B'
Waiting for the operation to be included...
Operation found in block: BLSwQ2yxRKnpLJMRwG7g6wiqZD7nXhAWpUVWP6KuFteJPoWJ82G (pass: 3, offset: 0)
This sequence of operations was run:
  Manager signed operations:
    From: tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ
    Fee to the baker: ꜩ0.5
    Expected counter: 3721194
    Gas limit: 1520
    Storage limit: 277 bytes
    Balance updates:
      tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ ............. -ꜩ0.5
      fees(tz3eWQiHsd1mc4aATajSi1MVX4xM3sc1vFjh,167) ... +ꜩ0.5
    Transaction:
      Amount: ꜩ120.5
      From: tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ
      To: tz1V2PiBobwPxfZmY3Gbja5zX8qdKjn3T2Lk
      This transaction was successfully applied
      Consumed gas: 1420
      Balance updates:
        tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ ... -ꜩ120.5
        tz1V2PiBobwPxfZmY3Gbja5zX8qdKjn3T2Lk ... +ꜩ120.5
        tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ ... -ꜩ0.06425

The operation has only been included 0 blocks ago.
We recommend to wait more.
Use command
  tezos-client wait for ooXoUpV6mF6yqeUbwX46BM8RSiLV2iAkmodhtjuaUSLcekX5W5B to be included --confirmations 5 --branch BLJueUguR75LVxPaqmP1zeE1JGL31Muz9KgyjLjaf73wEwfrsv1
and/or an external block explorer.
```

This prints a nice receipt for us detailing exactly what happened to our tez.

Let's take a look at `bob`, or `tz1V2PiBobwPxfZmY3Gbja5zX8qdKjn3T2Lk` on the
Jakartanet explorer:

![](bob-account.png)

But wait, why is it `120.5` tez? Didn't we pay a fee of `0.5` tez? Take a look
at the balance update sections of the receipt again (I've added notes for which
address is `alice` and which is `bob`)

```
...
Balance updates:
  alice tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ ....... -ꜩ0.5
  fees(tz3eWQiHsd1mc4aATajSi1MVX4xM3sc1vFjh,167) ... +ꜩ0.5
  ...
  Balance updates:
    alice tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ ... -ꜩ120.5
    bob   tz1V2PiBobwPxfZmY3Gbja5zX8qdKjn3T2Lk ... +ꜩ120.5
    alice tz1gBfV5Bij4s7bRPpSxkWzoD1WW7r84wEMZ ... -ꜩ0.06425
```

The fees came out of `alices` account separately from the transaction. So
the total amount `alice` payed was `-ꜩ121.06425`

## Conclusion

This concludes our chapter on basic Tezos operations. Next chapter, we'll
integrate what we've learned here with the Michelson we learned in chapter I to
actually run a Michelson contract live on the Tezos blockchain!

## Exercises

1. The Tezos client comes with a manual, which you can access with

   ```
   tezos-client man
   ```

   This will by default output the manual formatted with colors.
   Use the manual to figure out what options to pass to the
   `client man` command to turn the colors off.

   The Unix tools `grep` and `less` may be useful here, and there is also
   a copy on the web: [http://tezos.gitlab.io/api/cli-commands.html
   ](http://tezos.gitlab.io/api/cli-commands.html)

2. Find the hashes of the Mainnet and Test net genesis blocks. How do
   they differ?

3. Generate four new addresses in your client called `eenie`, `meenie`, `meinie`
   and `mo`. Conduct transfers to each address such that `eenie` has exactly
   `1` tez, `meenie` has exactly 2 tez, `meinie` has exactly `3` and `mo` has
   exactly `4`.


 [mainnet-expl]: https://tzkt.io/
 [jakartanet-expl]: https://jakartanet.tzkt.io/
 [docs-testnetworks]: http://tezos.gitlab.io/introduction/test_networks.html
